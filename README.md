# HIFIS Newsletter

contains the finished texts, pictures etc. for each newsletter issue

## für Oktoberausgabe beachten

* Anmeldelink aufnehmen in Mail
* Links in der HTML-Ausgabe händisch zurückändern vor Versand


## generelle Aspekte
* QR-Code zum Verteilen via Poster, Postkarte, etc.: Linkzhiel Subscribe to Newsletter
https://gitlab.hzdr.de/hifis/overall/templates/-/blob/master/qr_codes/hifis-newsletter-subscribe.svg


## Listenentwicklung
* ~~Subscribers vor dem ersten Newsletter (26.7.2022): 110~~
* ~~Nach Versand, vor Blogpost: (29.7.2022): 133~~
* ~~24.8.2022: 152~~
* now done [here](https://gitlab.hzdr.de/hifis/overall/kpi/newsletter)
* plot to be found [here](https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci/-/jobs/688564/artifacts/file/plots/pdf/newsletter-plot_2.pdf)


## Farbregeln Newsletter
auf Kontrast gemäß Web Content Accessibility Guidelines (WCAG) geprüft


**Hintergrund**
weiß oder $color-content-light-gray: #E0EAF2;


**Überschriften**
fett, 18, $color-helmholtz-blue: #005AA0;


**Schlagwörter**
fett, 14, schwarz


**Links**
normal, $color-helmholtz-blue: #005AA0;

## Trenner
2 px durchgezogener Strich, $color-helmholtz-blue: #005AA0; innerhalb von Themen gepunktet

 

## Pictures

all pictures, that might be used in Newsletters, need to be uploaded here as you can't keep pictures from elements you delete or change. For new upload, everyone needs to be able to access the pictures.

