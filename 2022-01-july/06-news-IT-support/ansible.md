Ansible ist eine auf Open Source basierende IT-Automatisierungs-Engine, mit der Provisionierung, Konfigurationsmanagement, Anwendungs-Deployment, Orchestrierung und viele weitere IT-Prozesse automatisiert werden können.
 Das HIFIS Software-Technology-Team stellt die entwickelten Rollen zum Betrieb der Dienste GitLab, Mattermost oder das Research Software Directory als Open Source Software unter einer freien Lizenz zur Verfügung.
Interessierte Dienstbetreiber sind gerne eingeladen, diese nachzunutzen oder sich sogar aktiv an der Entwicklung zu beteiligen.

Ansible is an open source IT automation engine.
With this, you can automate processes like provisioning, configuration management, deployment or orchestration of software programs.
The HIFIS Software Technology Team developed so called roles for the management of services like GitLab, Mattermost and the Research Software Directory. 
Those are reusable as open source software and can be found in a [GitHub repository](https://github.com/hifis-net).
Interested Service Providers are more then welcome to reuse them or take active part in the further developement!
