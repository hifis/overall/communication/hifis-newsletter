Dear HIFIS Community and Friends,


here it is, the first newsletter of HIFIS — Helmholtz Federated IT Services! 
Some may ask, why so late, HIFIS has been around for quite a while? 
Well, the HIFIS team initially focused on building great services for science.

Now we have reached a milestone that makes us proud: 
we have registered an [incredible 10,000 users of our services in the Helmholtz AAI](https://www.hifis.net/news/2022/07/13/10k-aai-users.html) by July 2022, 
including over 1,000 partners from other institutes and universities!

We will now report regularly on news from the HIFIS world and 
Sam, a young scientist somewhere in Helmholtz, will join us and hopefully give us interesting insights into practical use cases.


Enjoy reading and using HIFIS, the HIFIS Management Team!
