**GitLab — Helmholtz Codebase switches its domain:**
On November 11th, 2022, Helmholtz Codebase will be available under the new domain <https://codebase.helmholtz.cloud>.
The previous domain <https://gitlab.hzdr.de> will redirect to this new domain.
Nothing else changes for you, except for the need to log in once again.

We decided to reflect the fact that the vast majority of users
does not come from the HZDR any longer in the choice of the primary domain name.

[Read more...](https://hifis.net/news/2022/08/09/new-codebase-domain.html)
