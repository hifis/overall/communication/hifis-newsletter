You can find our compulsory information
and data privacy policy [here](www.desy.de/imprint/index_eng.html).


Imprint
Editorial Responsibility:
Lisa Klaffki, Jenny Engländer, HIFIS Communication Officers
support@hifis.net
HIFIS Coordination: Uwe Jandt
uwe.jandt@desy.de
Deutsches Elektronen-Synchrotron DESY
Notkestraße 85
D-22607 Hamburg
